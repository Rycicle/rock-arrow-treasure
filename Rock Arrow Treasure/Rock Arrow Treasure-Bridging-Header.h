//
//  Use this file to import your target's public headers that you would like to expose to Swift.
//

@import SpriteKit;

@interface SKSpriteNode ()

- (instancetype)initWithImageNamed:(NSString *)name NS_DESIGNATED_INITIALIZER;

@end

