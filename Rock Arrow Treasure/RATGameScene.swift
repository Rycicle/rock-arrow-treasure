//
//  RATGameScene.swift
//  Rock Arrow Treasure
//
//  Created by Ryan Salton on 17/04/2015.
//  Copyright (c) 2015 Ryan Salton. All rights reserved.
//

import SpriteKit

class RATGameScene: SKScene {
    
    let bgSky:SKSpriteNode = SKSpriteNode(imageNamed: "bg-sky")
    let bgGrassTop:SKSpriteNode = SKSpriteNode(imageNamed: "bg-grass")
    let bgGrassBottom:SKSpriteNode = SKSpriteNode(imageNamed: "bg-ground")
    
    let playerOne: SKSpriteNode = SKSpriteNode(color: UIColor.redColor(), size: CGSizeMake(80, 120))
    
    override init(size: CGSize) {
        super.init(size: size)
        
        self.backgroundColor = SKColor(red: 0.0/255.0, green: 217.0/255.0, blue: 217.0/255.0, alpha: 1.0)
        
        bgSky.position = CGPointMake(self.size.width * 0.5, self.size.height - bgSky.size.height * 0.5)
        self.addChild(bgSky)
        
        bgGrassBottom.position = CGPointMake(self.size.width * 0.5, bgGrassBottom.size.height * 0.5)
        self.addChild(bgGrassBottom)
        
        bgGrassTop.position = CGPointMake(self.size.width * 0.5, bgGrassBottom.size.height + (bgGrassTop.size.height * 0.5))
        self.addChild(bgGrassTop)
        
//        playerOne.color = UIColor.redColor()
        playerOne.position = CGPointMake(self.size.width * 0.5, bgGrassTop.position.y)
        self.addChild(playerOne)
    }

    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func didMoveToView(view: SKView) {
        /* Setup your scene here */
        
        
        
    }
    
    override func touchesBegan(touches: Set<NSObject>, withEvent event: UIEvent) {
        /* Called when a touch begins */
        
        for touch in (touches as! Set<UITouch>) {
            let location = touch.locationInNode(self)
            
            
        }
    }
    
    override func update(currentTime: CFTimeInterval) {
        /* Called before each frame is rendered */
    }

}
